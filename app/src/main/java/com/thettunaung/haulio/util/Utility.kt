@file:Suppress("DEPRECATION")

package com.thettunaung.haulio.util

import android.content.Context
import android.net.ConnectivityManager
import android.provider.Settings
import android.text.TextUtils

object Utility {
    fun isLocationEnabled(context: Context): Boolean {
        val locationProviders: String = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
        return !TextUtils.isEmpty(locationProviders)
    }

    fun isNetworkAvailable(context: Context): Boolean {
        val networkTypes = intArrayOf(ConnectivityManager.TYPE_MOBILE, ConnectivityManager.TYPE_WIFI)
        try {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            for (networkType in networkTypes) {
                val activeNetworkInfo = connectivityManager.activeNetworkInfo
                if (activeNetworkInfo != null &&
                        activeNetworkInfo.type == networkType) return true
            }
        } catch (e: Exception) {
            return false
        }
        return false
    }
}