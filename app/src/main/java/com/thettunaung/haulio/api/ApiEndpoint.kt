package com.thettunaung.haulio.api

import com.thettunaung.haulio.BuildConfig
import com.thettunaung.haulio.model.Job
import retrofit2.Call
import retrofit2.http.GET
import java.util.ArrayList


interface ApiEndpoint {
    @GET(BuildConfig.JOB_LIST_ENDPOINT)
    fun getJobList(): Call<ArrayList<Job>>
}
