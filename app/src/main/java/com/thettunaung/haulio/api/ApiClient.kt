package com.thettunaung.haulio.api

import com.thettunaung.haulio.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class ApiClient private constructor() {
    private val mService: ApiEndpoint

    init {
        val restAdapter = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create()).client(getNewHttpClient()).build()
        mService = restAdapter.create(ApiEndpoint::class.java)
    }

    private fun getNewHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder().followRedirects(true)
                .followSslRedirects(true)
                .retryOnConnectionFailure(true)
                .cache(null).apply {
                    val httpLoggingInterceptor = HttpLoggingInterceptor()
                    addInterceptor(httpLoggingInterceptor.apply { httpLoggingInterceptor.level = Level.BODY })
                    connectTimeout(10, TimeUnit.SECONDS)
                    followRedirects(true)
                    followSslRedirects(true)
                    writeTimeout(60, TimeUnit.SECONDS)
                    readTimeout(60, TimeUnit.SECONDS)
                    connectTimeout(60, TimeUnit.SECONDS)
                            .writeTimeout(60, TimeUnit.SECONDS)
                            .readTimeout(60, TimeUnit.SECONDS)
                }
        return builder.build()
    }


    companion object {
        private var instance: ApiClient? = null
        @Synchronized
        fun getService(): ApiEndpoint {
            return getRestInstance().mService
        }

        private fun getRestInstance(): ApiClient {
            if (instance == null) {
                instance = ApiClient()
            }
            return instance as ApiClient
        }
    }
}