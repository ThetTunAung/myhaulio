package com.thettunaung.haulio.model

import android.os.Build
import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.RequiresApi
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Job() : Parcelable {
  @SerializedName("id")
  @Expose
  var id: Int = 0
  @SerializedName("job-id")
  @Expose
  var jobId: Int = 0
  @SerializedName("priority")
  @Expose
  var priority: Int = 0
  @SerializedName("company")
  @Expose
  var company: String = ""
  @SerializedName("address")
  @Expose
  var address: String = ""
  @SerializedName("geolocation")
  @Expose
  var geolocation: Geolocation? = null

  @RequiresApi(Build.VERSION_CODES.M)
  constructor(parcel: Parcel) : this() {
    id = parcel.readInt()
    jobId = parcel.readInt()
    priority = parcel.readInt()
    company = parcel.readString()!!
    address = parcel.readString()!!
    geolocation = parcel.readParcelable(Geolocation::class.java.classLoader)
  }

  @RequiresApi(Build.VERSION_CODES.M)
  override fun writeToParcel(parcel: Parcel, flags: Int) {
    parcel.writeInt(id)
    parcel.writeInt(jobId)
    parcel.writeInt(priority)
    parcel.writeString(company)
    parcel.writeString(address)
    parcel.writeParcelable(geolocation,flags)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object CREATOR : Parcelable.Creator<Job> {
    @RequiresApi(Build.VERSION_CODES.M)
    override fun createFromParcel(parcel: Parcel): Job {
      return Job(parcel)
    }

    override fun newArray(size: Int): Array<Job?> {
      return arrayOfNulls(size)
    }
  }

}