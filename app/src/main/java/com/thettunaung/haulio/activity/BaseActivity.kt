package com.thettunaung.haulio.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.annotation.Nullable
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks
import com.thettunaung.haulio.R
import com.thettunaung.haulio.util.Utility
import kotlinx.android.synthetic.main.fake_toolbar.*


abstract class BaseActivity : AppCompatActivity() {

    private lateinit var googleApiClient: GoogleApiClient

    @LayoutRes
    protected abstract fun contentView(): Int

    protected abstract fun isShowBack(): Boolean

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(contentView())

        val signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
        googleApiClient = GoogleApiClient.Builder(this).enableAutoManage(this) { Toast.makeText(this@BaseActivity, "Logout failed try again later.", Toast.LENGTH_SHORT).show() }.addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions).build()

        if (isShowBack()) {
            back.visibility = View.VISIBLE
        } else {
            back.visibility = View.GONE
        }

        back.setOnClickListener {
            super.onBackPressed()
            finish()
        }

        logout.setOnClickListener {
            confirmLogout()
        }

    }

    private fun confirmLogout() {
        AlertDialog.Builder(this)
                .setMessage("Are you sure want to Logout?")
                .setCancelable(false)
                .setNeutralButton("No") { _, _ -> }
                .setPositiveButton("Yes") { _, _ ->
                    if (Utility.isNetworkAvailable(this)) {
                        googleApiClient.connect()
                        googleApiClient.registerConnectionCallbacks(object : ConnectionCallbacks {
                            override fun onConnected(@Nullable bundle: Bundle?) {
                                Auth.GoogleSignInApi.signOut(googleApiClient).setResultCallback {
                                    val intent = Intent(this@BaseActivity, SignInActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                }
                            }

                            override fun onConnectionSuspended(i: Int) {}
                        })
                    } else {
                        Toast.makeText(this, resources.getString(R.string.network_error), Toast.LENGTH_SHORT).show()
                    }
                }.show()
    }


}
