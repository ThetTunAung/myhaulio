package com.thettunaung.haulio.activity

import android.Manifest.permission
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sembozdemir.permissionskt.askPermissions
import com.thettunaung.haulio.R
import com.thettunaung.haulio.adapter.AddressSearchAdapter
import com.thettunaung.haulio.model.Job
import com.thettunaung.haulio.util.Utility
import kotlinx.android.synthetic.main.activity_map.*

@Suppress("UNCHECKED_CAST")
class MapActivity : BaseActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var jobObject: Job
    private var currentLocation: Location? = null
    private val markerList: ArrayList<Marker> = ArrayList<Marker>()

    override fun contentView(): Int {
        return R.layout.activity_map
    }

    override fun isShowBack(): Boolean {
        return true
    }

    override fun onResume() {
        super.onResume()
        checkLocation()
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        jobObject = intent.extras!!.get("job") as Job

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        setProfile()
        setAutoComplete()

    }

    private fun checkLocation() {
        askPermissions(permission.ACCESS_FINE_LOCATION, permission.ACCESS_COARSE_LOCATION) {
            onGranted {
                if(!Utility.isLocationEnabled(this@MapActivity)){
                    ensureLocationSettings()
                }else{
                    getLastKnownLocation()
                }
            }

            onDenied {

            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setProfile() {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (!account?.isExpired!!) {

            Glide.with(this).asBitmap().load(account.photoUrl).diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.ic_user)
                    .error(R.drawable.ic_user)
                    .into(user_profile)

            user_name.text = account.displayName
            job_number.text = getString(R.string.job_number) + jobObject.jobId.toString()
        } else {
            val intent = Intent(this, SignInActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun setAutoComplete() {
        val list: ArrayList<Job> = intent.extras!!.get("job_list") as ArrayList<Job>
        val adapter = AddressSearchAdapter(this, R.layout.address_item, list)
        autocomplete.setAdapter(adapter)
        autocomplete.onItemClickListener = AdapterView.OnItemClickListener { parent, arg1, pos, id ->
            val selected = adapter.getItem(pos)
            updateViewUI(selected!!)
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateViewUI(job: Job) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(autocomplete.windowToken, 0)
        autocomplete.clearFocus()
        autocomplete.text.clear()
        job_number.text = getString(R.string.job_number) + job.jobId.toString()


        mMap.clear()

        val latLng = LatLng(job.geolocation!!.latitude, job.geolocation!!.longitude)

        markerList.add(mMap.addMarker(
                MarkerOptions().position(
                        latLng
                ).title("Job location")
        )
        )

        markerList.add(mMap.addMarker(
                MarkerOptions().position(LatLng(currentLocation!!.latitude, currentLocation!!.longitude)).title(
                        "Current location"
                )
        ))

        val builder = LatLngBounds.Builder()

        markerList.apply {
            Log.d("MarkerList", "List " + markerList.size)
            //if(markerList.size == 2) {
            markerList.forEach {
                builder.include(it.position)
            }

            val bounds = builder.build()
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200))
        }

    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }


    private fun getLastKnownLocation() {
        fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (null != location) {

                        Log.d("getLastKnownLocation", "BBBBB")

                        currentLocation = location

                        markerList.add(mMap.addMarker(
                                MarkerOptions().position(LatLng(location.latitude, location.longitude)).title(
                                        "Current location"
                                )))


                        val latLng = LatLng(jobObject.geolocation!!.latitude, jobObject.geolocation!!.longitude)

                        markerList.add(mMap.addMarker(
                                MarkerOptions().position(
                                        latLng
                                ).title("Job location")))


                        val builder = LatLngBounds.Builder()

                        markerList.apply {
                            Log.d("MarkerList", "List " + markerList.size)
                            markerList.forEach {
                                builder.include(it.position)
                            }

                            val bounds = builder.build()
                            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 200))
                        }

                        rlLoading.visibility = View.GONE

                    }
                }
    }

    private fun ensureLocationSettings() {
        val builder = AlertDialog.Builder(this@MapActivity)
        builder.setMessage("Please enable \"Location\" service and set it to \"High Accuracy\" mode")
        builder.setCancelable(false)
        builder.setPositiveButton("Open location settings") { _, _ ->
            val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(myIntent)
        }
        builder.setNegativeButton("Cancel") { _, _ ->
            finish()
            moveTaskToBack(true)
        }
        builder.create().show()
    }

}
