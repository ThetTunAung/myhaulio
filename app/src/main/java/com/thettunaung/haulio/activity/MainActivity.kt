package com.thettunaung.haulio.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.thettunaung.haulio.R
import com.thettunaung.haulio.adapter.JobAdapter
import com.thettunaung.haulio.adapter.SpacesItemDecoration
import com.thettunaung.haulio.api.ApiClient
import com.thettunaung.haulio.model.Job
import com.thettunaung.haulio.util.Utility
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : BaseActivity() {
    private lateinit var adapter: JobAdapter
    private val timeExit = 2000L
    private var doubleBackPressed = false

    override fun contentView(): Int {
        return R.layout.activity_main
    }

    override fun isShowBack(): Boolean {
        return false
    }

    override fun onBackPressed() {
        if (doubleBackPressed) {
            super.onBackPressed()
            return
        }
        doubleBackPressed = true
        Toast.makeText(this, getString(R.string.press_back_exit), Toast.LENGTH_SHORT).show()
        Handler().postDelayed({ doubleBackPressed = false }, timeExit)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(Utility.isNetworkAvailable(this)) {
            fetchDataFromServer()
        }else{
            Toast.makeText(this,resources.getString(R.string.network_error),Toast.LENGTH_SHORT).show()
        }
    }

    private fun fetchDataFromServer() {
        val jobList = ApiClient.getService()
                .getJobList()
        jobList.enqueue(object : Callback<ArrayList<Job>> {
            override fun onResponse(call: Call<ArrayList<Job>>, response: Response<ArrayList<Job>>) {
                if (response.isSuccessful && response.body()!!.isNotEmpty()) {
                    Log.d("fetching", "data " + response.body()!!.size)
                    response.body().apply {
                        mProgress.visibility = View.GONE
                        job_list.visibility = View.VISIBLE
                        initAdapter(this!!)
                    }
                } else {
                    mProgress.visibility = View.GONE
                    mTextStatus.visibility = View.VISIBLE
                }
            }

            override fun onFailure(call: Call<ArrayList<Job>>, t: Throwable) {
                Log.e("fetch_error", "error " + t.message)
                t.printStackTrace()
                mProgress.visibility = View.GONE
                mTextStatus.visibility = View.VISIBLE
            }
        })
    }

    private fun initAdapter(dataList: ArrayList<Job>) {
        job_list.layoutManager = LinearLayoutManager(this@MainActivity)
        adapter = JobAdapter(dataList, object : JobAdapter.JobAdapterListener {
            override fun onItemSelected(clickedItem: Job) {
                if(Utility.isNetworkAvailable(this@MainActivity)) {
                    val intent = Intent(this@MainActivity, MapActivity::class.java)
                    intent.putExtra("job", clickedItem)
                    intent.putParcelableArrayListExtra("job_list", dataList)
                    startActivity(intent)
                }else{
                    Toast.makeText(this@MainActivity,resources.getString(R.string.network_error),Toast.LENGTH_SHORT).show()
                }
            }
        })
        job_list.adapter = adapter
        job_list.addItemDecoration(SpacesItemDecoration(16))
    }

}
