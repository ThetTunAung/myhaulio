package com.thettunaung.haulio.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.recyclerview.widget.RecyclerView
import com.thettunaung.haulio.R
import com.thettunaung.haulio.model.Job

class JobAdapter(private val userList: List<Job>,private val listener:JobAdapterListener): RecyclerView.Adapter<JobAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
        return userList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){
        var jobNumber: TextView = itemView.findViewById<View>(R.id.job_number) as TextView
        var address: TextView = itemView.findViewById<View>(R.id.address) as TextView
        var companyName: TextView = itemView.findViewById<View>(R.id.company_name) as TextView
        var accept: AppCompatButton = itemView.findViewById<View>(R.id.accept) as AppCompatButton

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.job_item, parent, false)
        return ViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = userList[position]

        holder.jobNumber.text = holder.itemView.context.getString(R.string.job_number)+item.jobId.toString()

        holder.address.text = holder.itemView.context.getString(R.string.address)+item.address
        holder.companyName.text = holder.itemView.context.getString(R.string.company_name)+item.company
        holder.accept.setOnClickListener {
           listener.onItemSelected(item)
        }
    }

    interface JobAdapterListener {
        fun onItemSelected(clickedItem : Job)
    }

}