package com.thettunaung.haulio.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.thettunaung.haulio.R;
import com.thettunaung.haulio.model.Job;

import java.util.ArrayList;

public class AddressSearchAdapter extends ArrayAdapter<Job> {
    private Context context;
    private ArrayList<Job> items, tempItems, suggestions;

    public AddressSearchAdapter(@NonNull Context context, int resourceId, ArrayList<Job> items) {
        super(context, resourceId, items);
        this.items = items;
        this.context = context;
        tempItems = new ArrayList<>(items);
        suggestions = new ArrayList<>();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.address_item, null);

            TextView tv = convertView.findViewById(R.id.tvAddress);

            Job job = getItem(position);
            assert job != null;
            tv.setText(job.getAddress());
        }

        return convertView;

    }

    @Nullable
    @Override
    public Job getItem(int position) {
        return items.get(position);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public Filter getFilter() {
        return fruitFilter;
    }

    private Filter fruitFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Job fruit = (Job) resultValue;
            return fruit.getAddress();
        }

        @Override
        protected FilterResults performFiltering(CharSequence charSequence) {
            if (charSequence != null) {
                suggestions.clear();
                for (Job fruit : tempItems) {
                    if (fruit.getAddress().toLowerCase().startsWith(charSequence.toString().toLowerCase())) {
                        suggestions.add(fruit);
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
            ArrayList<Job> tempValues = (ArrayList<Job>) filterResults.values;
            if (filterResults.count > 0) {
                clear();
                for (Job fruitObj : tempValues) {
                    add(fruitObj);
                    notifyDataSetChanged();
                }
            } else {
                clear();
                notifyDataSetChanged();
            }
        }
    };
}